'use strict';

var restify = require('restify');

// Global variables...
global.mongoose = require('mongoose');
global.config = require('konfig')();

var app = restify.createServer({
    name: 'Eazy',
    version: '1.0.0'
});
app.use(restify.queryParser());
app.use(restify.bodyParser());
app.use(restify.CORS());


//Connect to MongoDB.......
mongoose.connect(config.db.url, {user: config.db.user, pass: config.db.pass, port: config.db.port}, function (err) {
    if (err) {
        console.log("DB Error: ", err);
        process.exit(1);
    } else {
        console.log('MongoDB Connected');
    }
});

require('./routes/index')(app);

app.listen(config.app.port, function(){
    console.log('Server started at: http://127.0.0.1:' + config.app.port);
});

module.exports = app;