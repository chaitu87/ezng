# Project Ezng

## Setup your repo

##### npm install ( some of you have to do 'sudo npm install' depending on your environment )
##### run mongodb

##Routes active in eaZing

### User actions

1. List all Users (GET /users)
2. Show user info (GET /users/:id)
3. Create a user (POST /users)
4. Update a user (PUT /user/:id)
5. Delete a user (DELETE /user/:id)

### Ticket actions
1. Show a tickets (GET /tickets)
2. Create a ticket (POST /tickets)
3. Get a ticket info (GET /ticket/:id)

### 

6. Sign in a user (POST /login)