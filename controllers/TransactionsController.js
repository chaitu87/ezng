'use strict';

var exports = module.exports = {};
var Transaction = require('../models/Transaction');

function store(req, res) {
    var newTransaction = new Transaction();
    newTransaction.userId = req.params.userId;
    newTransaction.mallName = req.params.mallName;
    newTransaction.gateType = req.params.gateType;
    newTransaction.gateNumber = req.params.gateNumber;
    if (req.params.isEntry == true) {
        newTransaction.inTime = Date.now();
    } else {
        newTransaction.outTime = Date.now();
    }
    newTransaction.vehicleNumber = req.params.vehicleNumber;
    newTransaction.save(function(err, data) {
        if (err) throw err;
        res.send({
            mallName: data.mallName,
            ticket: {
                inTime: data.inTime,
                outTime: data.outTime,
                id: data._id
            }
        });
    });
}

module.exports = {
    store: store
};