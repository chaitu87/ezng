'use strict';

var exports = module.exports = {};
var Pos = require('../models/Pos');

function store(req, res) {
    var newPos = new Pos();
    newPos.userId = req.params.userId;
    newPos.mallName = req.params.mallName;
    newPos.gateType = req.params.gateType;
    newPos.gateNumber = req.params.gateNumber;
    if (req.params.isEntry == true) {
        newPos.inTime = Date.now();
    } else {
        newPos.outTime = Date.now();
    }
    newPos.vehicleNumber = req.params.vehicleNumber;
    newPos.save(function(err, data) {
        if (err) throw err;
        res.send({
            mallName: data.mallName,
            ticket: {
                inTime: data.inTime,
                outTime: data.outTime,
                id: data._id
            }
        });
    });
}

module.exports = {
    store: store
};