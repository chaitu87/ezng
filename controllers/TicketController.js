'use strict';

var exports = module.exports = {};
var Ticket = require('../models/Ticket');

function store(req, res) {
    var newTicket = new Ticket();
    newTicket.userId = req.params.userId;
    newTicket.mallName = req.params.mallName;
    newTicket.gateType = req.params.gateType;
    newTicket.gateNumber = req.params.gateNumber;
    if (req.params.isEntry == true) {
        newTicket.inTime = Date.now();
    } else {
        newTicket.outTime = Date.now();
    }
    newTicket.vehicleNumber = req.params.vehicleNumber;
    newTicket.save(function(err, data) {
        if (err) throw err;
        res.send({
            mallName: data.mallName,
            ticket: {
                inTime: data.inTime,
                outTime: data.outTime,
                id: data._id
            }
        });
    });
}

function list(req, res) {
    var list = Ticket.find().populate('userId').exec(function(err, Tickets) {
        if (err) throw err;
        console.log(Tickets);
        res.send({
            Tickets: Tickets
        });
    });
}

function check(req, res) {
    Ticket.find({
        userId: req.params.id
    }, function(err, Tickets) {
        if (err) throw err;
        if (Tickets.length > 0) {
            res.send({
                isChecked_In: true,
                ticket: Tickets // Change this as per ticket strategy
            });
        } else {
            res.send({
                isChecked_In: false
            });
        };
    })
}

module.exports = {
    store: store,
    list: list,
    check: check
};
