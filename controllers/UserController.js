'use strict';

var exports = module.exports = {};
var User = require('../models/User');
var md5 = require('md5');

function store(req, res) {
    var newuser = new User();
    newuser.name =  req.params.name;
    newuser.mobile = req.params.mobile;
    newuser.email = req.params.email;
    newuser.profile_img = req.params.profile_img;
    newuser.password = md5(req.params.password);
    newuser.save(function(err, data) {
        if (err) throw err;
        res.send({
            id: data._id,
            name: data.name,
            mobile: data.mobile,
            email: data.email,
            profile_img: data.profile_img
        });
    });
}

function list(req, res) {
    var list = User.find({},'name mobile email profile_img created_at updated_at', function(err, users) {
        if (err) throw err;
        console.log(users);
        res.send({
            users: users
        });
    });
}

function show(req, res) {
    User.find({
        _id: req.params.id
    },'name mobile email profile_img created_at updated_at', function(err, user) {
        if (err) throw err;
        // object of the user
        res.send(user[0]);
    });
}

function update(req, res) {

}

function destroy(req, res) {

}

function login(req, res) {
    User.find({
        email: req.params.email,
        password: md5(req.params.password)
    }, function(err, users) {
        if (err) throw err;
        if (users.length > 0) {
            res.send({
                message: "Successfully Logged In",
                data: {
                    _id: users[0]._id,
                    updated_at: users[0].updated_at,
                    created_at: users[0].created_at,
                    profile_img: users[0].profile_img,
                    email: users[0].email,
                    mobile: users[0].mobile,

                }
            });
        } else {
            res.send({
                message: "Incorrect Password"
            });
        };
    })
}

module.exports = {
    store: store,
    list: list,
    show: show,
    update: update,
    destroy: destroy,
    login: login
};
