'use strict';

var exports = module.exports = {};
var Vendor = require('../models/Vendor');

function store(req, res) {
    var newVendor = new Vendor();
    newVendor.userId = req.params.userId;
    newVendor.mallName = req.params.mallName;
    newVendor.gateType = req.params.gateType;
    newVendor.gateNumber = req.params.gateNumber;
    if (req.params.isEntry == true) {
        newVendor.inTime = Date.now();
    } else {
        newVendor.outTime = Date.now();
    }
    newVendor.vehicleNumber = req.params.vehicleNumber;
    newVendor.save(function(err, data) {
        if (err) throw err;
        res.send({
            mallName: data.mallName,
            ticket: {
                inTime: data.inTime,
                outTime: data.outTime,
                id: data._id
            }
        });
    });
}

module.exports = {
    store: store
};