var Schema = mongoose.Schema;

var Pos = new Schema({
    name: {
    	type: String,
    	required: true
    },
    mobile: {
        type: String
    },
    email: {
        type: String,
        required: true
    },
    profile_img: {
        type: String
    },
    password: {
        type: String
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
module.exports = mongoose.model('Pos', Pos);