var Schema = mongoose.Schema;

var Ticket = new Schema({
    userId: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    inTime: {
        type: Date,
		default: Date.now()
    },
    outTime: {
        type: Date
    },
    vehicleNumber: {
        type: String,
		required: true
    },
    pos:{
        type: Schema.Types.ObjectId,
		ref: 'Pos'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
module.exports = mongoose.model('Ticket', Ticket);