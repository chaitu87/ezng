var Schema = mongoose.Schema;

var Transaction = new Schema({
    transid:{
        type: "String",
		required: true
    },
	amount:{
		type: "Number",
		required: true
	},
	remark:{
		type: "String"
	},
	isSuccess:{
		type: "Boolean",
		required: true,
		default: false
	}
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
module.exports = mongoose.model('Transaction', Transaction);