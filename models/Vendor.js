var Schema = mongoose.Schema;

var Vendor = new Schema({
    name: {
    	type: String,
    	required: true
    },
    mobile: {
        type: String,
		required: true
    },
    email: {
        type: String,
        required: true
    },
    profile_img: {
        type: String
    },
    password: {
        type: String
    },
    address: {
		type: String,
		required: true
	},
	poss: [{
		type: Schema.Types.ObjectId,
		ref: 'Pos'
	}]
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
module.exports = mongoose.model('Vendor', Vendor);