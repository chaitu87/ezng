module.exports = function(app) {
    var PosController = require('../controllers/PosController');
    app.post('/tickets', PosController.store);
    app.get('/tickets', PosController.list);
    app.get('/checkuser/:id', PosController.check);
    // app.get('/ticket/:id', TicketController.show);
};