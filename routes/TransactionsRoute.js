module.exports = function(app) {
    var TransactionsController = require('../controllers/TransactionsController');
    app.post('/tickets', TransactionsController.store);
    app.get('/tickets', TransactionsController.list);
    app.get('/checkuser/:id', TransactionsController.check);
    // app.get('/ticket/:id', TransactionsController.show);
};