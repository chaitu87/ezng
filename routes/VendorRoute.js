module.exports = function(app) {
    var VendorController = require('../controllers/VendorController');
    app.post('/vendors', VendorController.store);
};