module.exports = function(app) {
    var UserController = require('../controllers/UserController');
    app.get('/users', UserController.list);
    app.get('/users/:id', UserController.show);
    app.post('/users', UserController.store);
    app.put('/users/:id', UserController.update);
    app.del('/users/:id', UserController.destroy);
    app.post('/login', UserController.login)
};