module.exports = function(app) {
    var TicketController = require('../controllers/TicketController');
    app.post('/tickets', TicketController.store);
    app.get('/tickets', TicketController.list);
    app.get('/checkuser/:id', TicketController.check);
    // app.get('/ticket/:id', TicketController.show);
};