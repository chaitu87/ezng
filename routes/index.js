module.exports = function (app) {
	app.get('/', function(req, res, next) {
		return res.send({
			message: "WELCOME TO EAZING API"
		})
	});
	require("./UsersRoute")(app);
	require("./TicketsRoute")(app);
	require("./VendorRoute")(app);
	require("./PosRoute")(app);
	require("./TransactionsRoute")(app);
};